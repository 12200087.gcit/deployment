import React, { useState,useRef} from "react";
import InputForm from "./InputForm"
import TodoList from "./TodoList";
import styles from './input.module.css'

function ToDoApp() {
const [todoList, setTodoList] = useState([]);
const [displayCompleted, setDisplayCompleted] = useState(true);
const nextId = useRef(0);

function addTodoItem(text) {
  const newTodoItem = {
  id: nextId.current++,
  text,
  completed: false,
  };
  setTodoList((prevTodoList) => [...prevTodoList, newTodoItem]);
  }
 

  function toggleCompleted(id) {
  setTodoList((prevTodoList) =>
  prevTodoList.map((todo) =>
  todo.id === id ? { ...todo, completed: !todo.completed } : todo
  )
  );
  }

  function clearCompleted() {
  setTodoList((prevTodoList) =>
  prevTodoList.filter((todo) => !todo.completed)
  );
  }

  return (
  <div className={styles.container}>
  
    <div>
    <h2 style={{marginLeft:'10px',
       fontFamily:'-moz-initial'}}>Todo List</h2>
      <InputForm style={{marginLeft:'10px'}} addTodoItem={addTodoItem} />
      <TodoList
          todoList={todoList}
          displayCompleted={displayCompleted}
          toggleCompleted={toggleCompleted}
          toggleDisplayCompleted={setDisplayCompleted}
          clearCompleted={clearCompleted}
    />

    </div> 
  </div>  

  );
}

export default ToDoApp;
import React,{useState} from 'react'

function InputForm(props){
    const [userInput,setUserInput]=useState('')

    function handleSubmit(event) {
        event.preventDefault();
        if (userInput.trim()) {
        props.addTodoItem(userInput);
        setUserInput("");
        }
      
        }
    return(
        <div >    
            <form onSubmit={handleSubmit}>
                <div>
                    <input style={{
                          
                          width: '75%',
                          padding:'7px 15px',
                          marginLeft:'10px',}} 
                      type="text"
                      placeholder="Add a new todo" 
                      value={userInput} 
                      onChange={(event)=>setUserInput(event.target.value)}></input>

                    <button style={{            
                      marginLeft:'5px',                    
                      color:'black',
                      padding:'7px 15px'}} type="submit">Add
                    </button>

                </div>
               
            </form>
        </div>
    )
}
export default InputForm